import unittest
from app import app, db, User

class BasicTests(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_home_page(self):
        response = self.app.get('/')
        self.assertEqual(response.status_code, 200)

   def test_register_page(self):
       response = self.app.get('/register')
       self.assertEqual(response.status_code, 200)

   def test_user_registration(self):
       response = self.app.post('/register', data=dict(username="testuser"), follow_redirects=True)
       self.assertEqual(response.status_code, 200)

       # Vérifier si l'utilisateur a été créé
       user = User.query.filter_by(username="testuser").first()
       self.assertIsNotNone(user)

if name == 'main':
    unittest.main()